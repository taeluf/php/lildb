# LilDB Dev status

## Apr 23, 2023
bunch of work on bigdb & bigorm. Some new methods. Some bugfixes. Some api changes. 

I need to integrate the optional access layers, add tests, and probably add an `onWillSave()`, and maybe other triggers on the orm objects.

It's actually kind of functional now.

## March 16, 2023
fixed `bigdb->exec()` so multiquery works
started access layer & test for it
disabled tests that had no conditions defined for passing
made the multiexec test actually pass
figured out why scrawl documentation is not generating correctly for BigDb. It's because of a variable named `$namespace` inside a method's arglist. I couldn't fix the bug, idk, that's a problem for the lexer & code scrawl.


## March 12, 2023
I benchmarked using ArrayObject to hold a bunch of objects instead of an array, for, like, returning from query() or many(). The cost is negligible & it's okay to use ArrayObject, unless I expect 10,000 iterations of creating array lists with 200 objects in each, which would cost about 8ms more than just using arrays.

Though, it is questionable if I should make the lists the default, or if I should leave that up to the caller of query/many etc.

Features Added
- bigdb->many()
- bigorm->property_related() now stores the related object on `$_prop_name` and is (still) accessed via `$prop->name`. This allows for an orm class to rely on its own magic `__get()` as well as change the access level of the prop.


Began testing many()
made build_database() in the test class & turned testBuildDatabase into a real test
added testMultiQuery because I can't get multi-query to work through lilsql.


## March 11, 2023
- started BigDb class, added `load(query.key, class_basename)` & `load_queries(dir, namespace)`. Added `testIdk` to start working on it. I'll want a new Orm class. I like LilOrm kinda, but I want something differnt for BigDb/BigOrm

I added execution, refactored the test class a little bit, and added queries to create an article table & to insert test rows
Files:
- `code/BigDb` - central class putting it all together
- `test/run/BigDb.php` test class
- `test/input/bigdb-sql/Article.php` - Orm object
- `test/input/bigdb-sql/articles.sql` - sql queries
- `test/input/bigdb-sql/dv.queries` - serialized copy of queries
- `code/BigOrm` - the orm class to extend from

Features added:
- `$orm_obj->prop` will try `getProp()` then `property_related($prop)` 
- magic related lazy loading by setting `ClassName $related_item` and `int $related_item_id` properties.

## march 1, 2022
- started LilMigrations class for fun

## LilOrm
- Test it
- add features
- document it better

## LilSql
While the tests don't have any programmed pass/fail conditions, they are working as intended, from a visual inspection. I have not written sql in main.sql or update.sql, but both create.sql & delete.sql are loading as expected when running `pt LilSql`, which loads a file & serializes it before unserializing & outputting the results

## TODO (LilSql):
- clean up commented out code
- make the tests ACTUALLY PASS
- test exception when multiple queries of the same name
- document LilSql properly (write a test method & copy the method contents into the readme file)

## Other TODO
- add docs recommending pdo errmode exception for lildb
- at top-ish of readme, write overview of what this lib is/does (what each Lil class does)
- write proper documentation of LilDb & LilMigrations w/ examples (copied from test methods)


## NOTICE
code scrawl isn't working ... because of the lexer issues ... so basically just don't run code scrawl


## Next
- Write some really brief, simple tests

## Latest
- Updated code scrawl with a classMethod template & implemented it in the readme
- Define the methods I'd like to implement
- Iterate over each, document, & implement
