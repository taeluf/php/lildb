<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/LilSql.php  
  
# class Tlf\LilSql  
Facilitates storing SQL on-disk in sql files and converting those sql files to an array of commands & then serializing that.  
  
This is idealy a build tool, not a runtime tool, so you will `unserialize(file_get_contents(...))` to load the array of queries  
  
Or `@query(some.name, delimiter)` if you don't want to stop on a semi-colon  
  
See source code at [/code/LilSql.php](/code/LilSql.php)  
  
## Constants  
  
## Properties  
- `public $queries = [];` array of queries to use/serialize (you should serialize them & skip this processing in production!)  
  
## Methods   
- `public function load_files(string $dir, string $namespace_prefix='')` Load sql files from the following dir into the queries  
- `public function serialize(string $file)`   
- `public function parse_sql(string $sql, string $prefix='', &$queries  [])` Convert an SQL string containing multiple queries into an array. All queries must have an explicit identifier.  
  
  
