<?php

namespace Tlf\LilDb\Test;

class Migrations extends \Tlf\Tester {

    public function testMigrateUp1(){
        // @export_start(Example.Migration)
        $db = $this->file('test/input/migrate/db.sqlite');
        unlink($db);
        $migrations_dir = $this->file('test/input/migrate/');

        // init the database
        $ldb = \Tlf\LilDb::sqlite($db);
        $ldb->create('blog',
            ['title'=>'varchar(200)']
        );
        $ldb->insert('blog',['title'=>'one']);

        // do the migration
        $lm = new \Tlf\LilMigrations($ldb->pdo, $migrations_dir);
        // $lm->migration_vars = ['some_var'=> 'some value']; # optionally expose variables to the migration files.
        $lm->migrate(0,1);

        // test that the table has been altered to have a 'description' field
        $this->compare(
            $ldb->select('blog')[0],
            ['title'=>'one',
            'description'=>'',
            ],
        );
        // @export_end(Example.Migration)
    }


    public function testMigrateUp2(){
        $db = $this->file('test/input/migrate/db.sqlite');
        unlink($db);
        $migrations_dir = $this->file('test/input/migrate/');

        $ldb = \Tlf\LilDb::sqlite($db);
        $ldb->create('blog',
            ['title'=>'varchar(200)']
        );
        $ldb->insert('blog',['title'=>'one']);

        $lm = new \Tlf\LilMigrations($ldb->pdo, $migrations_dir);

        $lm->migrate(0,2);
        $this->compare(
            $ldb->select('blog')[0],
            ['title'=>'one',
            'description'=>'',
            'id'=>null,
            ],
        );

    }


    public function testMigrateDown2(){
        $db = $this->file('test/input/migrate/db.sqlite');
        unlink($db);
        $migrations_dir = $this->file('test/input/migrate/');

        $ldb = \Tlf\LilDb::sqlite($db);
        $ldb->create('blog',
            ['id'=>'int(11)', 'title'=>'varchar(200)', 'description'=>'varchar(300)']
        );
        $ldb->insert('blog',['title'=>'one']);

        $lm = new \Tlf\LilMigrations($ldb->pdo, $migrations_dir);

        $lm->migrate(3,1);
        $this->compare(
            ['title'=>'one'],
            $ldb->select('blog')[0],
        );

        // exit;

    }

}
