# LilDb: A little database wrapper for PDO
Remove a lot of boilerplate code with wrapper functions for common sql verbs.

This documentation is generated with @easy_link(tlf,php/code-scrawl). For the README template file see @see_file(.docsrc/README.src.md)

## Features
`LilSql` is new! See @see_file(test/run/Sql.php) for examples!
- `LilDb`: @ast(class.Tlf\LilDb.docblock.attribute.tagline.description)
- `LilMigrations`: @ast(class.Tlf\LilMigrations.docblock.attribute.tagline.description)
- `LilSql`: @ast(class.Tlf\LilSql.docblock.attribute.tagline.description)
- `LilOrm`: @ast(class.Tlf\LilOrm.docblock.attribute.tagline.description)

## Install
`composer require taeluf/lildb v0.1.x-dev`

## Usage
Instantiate LilDb or LilMigration & call the methods you need. For examples, see @see_file(test/run/Tests.php) and @see_file(test/run/Migrations.php)

## Example (lildb + migration)
```php
<?php
@import(Example.Migration)
```

Migration file `test/input/migrate/v1/up.sql`:
```sql
@file(test/input/migrate/v1/up.sql)
```

@ast(class.Tlf\LilDb, ast/class)

@ast(class.Tlf\LilMigrations, ast/class)

@ast(class.Tlf\LilSql, ast/class)

@ast(class.Tlf\LilOrm, ast/class)
