<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/LilDb.php  
  
# class Tlf\LilDb  
A lil tiny database class for common sql functions like CREATE, UPDATE, SELECT, DELETE  
  
See source code at [/code/LilDb.php](/code/LilDb.php)  
  
## Constants  
  
## Properties  
- `public \PDO $pdo;` a pdo instance  
  
## Methods   
- `static public function new(string $user, string $password, string $db, $host='localhost')` Convenience method to initialize with pdo  
- `static public function sqlite(string $dbName = ':memory:')` Convenience method to initialize sqlite db in memory  
- `static public function mysql($dbName = ':memory:')` Convenience method to initialize mysql db in memory  
- `public function __construct(\PDO $pdo)` Initialize with a db handle  
- `public function create(string $tableName, array $colDefinitions, bool $recreateIfExists=false)` Create a new table if it doesn't exist.  
  
- `public function query(string $sql, array $binds=[])` Execute an Sql statement & get rows back  
- `public function select(string $tableName, array $whereCols=[])` Get rows from a table with the given $whereCols  
- `public function insert(string $table, array $row)` Insert a row into the database  
Converts array values to json  
  
- `public function insertAll(string $table, array $rowSet)`   
- `public function update(string $table, array $newRowValues, string $idColumnName='id')` Update an existing row. Shorthand for `updateWhere()` with the id column set as the where values.  
- `public function updateWhere(string $table, array $newRowValues, array $whereVals)`   
- `public function delete(string $table, array $whereCols)` Delete rows from a table  
- `public function execute(string $sql, array $binds=[])` Execute an Sql statement & get a PDOStatement back  
- `public function exec(string $sql, array $binds=[])` Alias for `execute()`  
- `public function getPdo()` get the pdo object  
- `public function pdo()` get the pdo object  
- `static public function whereSqlFromCols(array $columns)` Convert key=>value array into a 'WHERE' sql.  
  
- `static public function keysToBinds(array $keyedValues)` Convert an array `['key'=>$val, ':key2'=>$val]` into binds: `[':key'=>$val, ':key2'=>$val]`.  
  
  
