<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/LilOrm.php  
  
# class Tlf\LilOrm  
Minimal ORM implementation.  
  
See source code at [/code/LilOrm.php](/code/LilOrm.php)  
  
## Constants  
  
## Properties  
- `public $_cache = [];`   
  
## Methods   
- `public function __construct($row)`   
- `public function one($name, $id, $id_column='id')` Get an object with the given id  
  
- `public function many($that, $id_column=null, $this_tablenull)` Get an array of objects that point to this item  
  
  
