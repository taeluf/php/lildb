<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/LilMigrations.php  
  
# class Tlf\LilMigrations  
A minimal class for handling sql migration. Create a migrations dir. Then create dirs like `v1`, `v2` & create files `up.sql`, `down.sql` in each versioned dir. Migrating from 1 to 2 will execute `v2/up.sql`. From 3 down to 1 will execute `v2/down.sql` and `v1/down.sql`. You may also make files like `v1/up-1.sql`, `v1/up-2.sql` to execute multiple files in order.  
  
You can create `.config/ldb.json` file with the relative `'dir'` and `'db'` settings  
  
See source code at [/code/LilMigrations.php](/code/LilMigrations.php)  
  
## Constants  
  
## Properties  
- `public \PDO $pdo;` a pdo instance  
- `public string $dir;` the dir for migrations scripts.  
- `public array $migration_vars = [];` array<string, mixed> of variables to `extract` and make available to `up.php` and `down.php` migration files.  
  
## Methods   
- `public function __construct(\PDO $pdo, string $dir)` In $dir, there should be directories named 'v1', 'v2', 'v3', and so on.  
In the v1/v2/v3 dirs, there should be up.sql & down.sql files with valid SQL statements for whatever database you're using  
  
- `static public function sqlite(string $dbName = ':memory:')` Convenience method to initialize sqlite db in memory  
- `public function migrate(int $old, int $new)` Migrate from old version to new  
  
- `public function run_migration_version($version, $up_or_down)`   
  
